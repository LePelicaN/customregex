﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace CustomRegex
{
    public class RegexLoader
    {
        private XElement _main;

        public RegexLoader()
        {
        }

        public RegexLoader(string fileName)
        {
            LoadFile(fileName);
        }

        private void LoadFile(string fileName)
        {
            _main = XElement.Load(@"WebSitesRegex/" + fileName);
        }

        public CustomRegexInfo Load(string fileName, string codeName)
        {
            LoadFile(fileName);
            return Load(codeName);
        }

        internal IEnumerable<string> LoadRemoveUntils(XElement part)
        {
            return part
                        .Descendants("removeuntil")
                        .Select(e => (string)e);
        }

        internal IEnumerable<string> LoadRemoveFroms(XElement part)
        {
            return part
                        .Descendants("removefrom")
                        .Select(e => (string)e);
        }

        internal IEnumerable<Tuple<string, string>> LoadReplacementRegexes(XElement part)
        {
            return part
                        .Descendants("replace")
                        .Select(e => Tuple.Create((string)e.Element("in"), (string)e.Element("out")));
        }

        internal Dictionary<string, string> LoadCodeToExp(XElement part)
        {
            return 
                part
                    .Descendants("var")
                    .ToDictionary(e => (string)e.Attribute("name"),
                                  e => (string)e);
        }

        internal string LoadMatchingRegex(XElement part)
        {
            Dictionary<string, string> codeToExp = LoadCodeToExp(part);
            return string.Join("",
                    ((string)part.Element("main"))
                        .Split()
                        .Select(s => codeToExp.ContainsKey(s) ? codeToExp[s] : s));
        }

        public CustomRegexInfo Load(string codeName)
        {
            try
            {
                var customRegex = new CustomRegexInfo();
                XElement part = 
                    _main
                        .Descendants ( "part" )
                        .First(e => (string)e.Attribute("name") == codeName);

                customRegex.RemoveUntils = LoadRemoveUntils(part);
                customRegex.RemoveFroms = LoadRemoveFroms(part);
                customRegex.RemplacementRegexes = LoadReplacementRegexes(part);
                customRegex.MatchingRegex = LoadMatchingRegex(part);

                return customRegex;
            }
            catch (Exception e)
            {
                throw new Exception( "Regex loading error: " + e.Message, e );
            }
        }
    }
}
