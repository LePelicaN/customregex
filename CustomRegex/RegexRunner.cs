﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CustomRegex
{
    public class RegexRunner
    {
        internal static string RemoveUntils(CustomRegexInfo customRegex, string text)
        {
            foreach (string removeUntil in customRegex.RemoveUntils)
            {
                var index = text.IndexOf(removeUntil, StringComparison.Ordinal);
                text = text.Remove(0, index + removeUntil.Length);
            }
            return text;
        }

        internal static string RemoveFroms(CustomRegexInfo customRegex, string text)
        {
            foreach (string removeFrom in customRegex.RemoveFroms)
            {
                var index = text.IndexOf(removeFrom, StringComparison.Ordinal);
                text = text.Remove(index);
            }
            return text;
        }

        internal static string ReplaceRegexes(CustomRegexInfo customRegex, string text)
        {
            foreach (Tuple<string, string> remplacement in customRegex.RemplacementRegexes)
            {
                text = Regex.Replace(text, remplacement.Item1, remplacement.Item2);
            }
            return text;
        }

        internal static IEnumerable<List<string>> Matching(CustomRegexInfo customRegex, string text)
        {
            var matches = Regex.Matches(text, customRegex.MatchingRegex, RegexOptions.Singleline);
            return matches.Cast<Match>()
                        .Take(Math.Max(10, matches.Count))
                        .Select(m => m
                            .Groups.Cast<Group>()
                            .Select(g => g.Value)
                            .ToList());
        }

        public static IEnumerable<List<string>> Run(CustomRegexInfo customRegex, string text)
        {
            try
            {
                var textModified = (string)text.Clone();
                textModified = RemoveUntils(customRegex, textModified);
                textModified = RemoveFroms(customRegex, textModified);
                textModified = ReplaceRegexes(customRegex, textModified);
                return Matching(customRegex, textModified);
            }
            catch(Exception e)
            {
                throw new Exception( "Regex running error: " + e.Message, e );
            }
        }
    }
}
